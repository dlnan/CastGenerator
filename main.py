#!/usr/bin/env python2
#coding:UTF-8


import sys
import os.path

#import re


#导入自定义的模块
from pymodules.name_list_handler import *
from pymodules.little_functions import *
from pymodules.page_drawer_pillow import *
#from pymodules.video_maker import *
from pymodules.config_handler import Configs




#生命各种变量
_VERSION=0.6

#文件相关
_PATHPREFIX='files/'
_CASTLISTFILE=_PATHPREFIX+'castlist.txt'
_PAGEFILE = _PATHPREFIX+'page.png'
_CONFIGFILE = _PATHPREFIX+'CastGenerator.conf'
_VIDEOFILE = _PATHPREFIX+'video.mpg'


#全局设置变量，字典
#_GLOBAL_SETTINGS = Get_configs()



class CastGenerator:
  def __init__(self) :
    self.conf = Configs(_CONFIGFILE)
    self.GLOBAL_SETTINGS = self.conf.configs
  def __main__(self,argvs):
    argv_dict={
      'help' : self.helpme,
      'page' : self.makepage,
      'video' : self.makevideo,
      'example' : self.makeexample,
      'config' : self.makeconfig
      }
    if len(argvs)==1 :
      argvs.append('help')
    argv_dict.get(argvs[1],self.helpme)()
    if argvs[1] != 'help' :
      Wait_for_press(True)
    
  #帮助
  def helpme(self,command=None):
    HELPLINE=u'''本程序用于制作电影片尾名单视频。
用法：
        CastGenerator help : 用于显示帮助
        CastGenerator page : 根据名单文本生成名单长图
        CastGenerator example : 生成名单文本范例文件
        CastGenerator video : 根据设置将长图生成视频
        CastGenerator config : 输出默认设置文件'''
    print(HELPLINE)
  
  #生成长图
  def makepage(self,command=None):
    if os.path.isfile(_CASTLISTFILE) :
      print(u'找到名单文件'+_CASTLISTFILE)
      castfile=Castfile(_CASTLISTFILE).formatedlines
      print(  u'正在将名单长图输出至'+_PAGEFILE )
      castpage = Cast_list_page(self.GLOBAL_SETTINGS)
      castpage.Get_lines(castfile)
      castpage.Draw_page()
      castpage.Save(_PAGEFILE)
    else:
      print(u'找不到名单文件。请查看帮助。')
      
  #生成默认名单文件
  def makeexample(self,command=None):
    make_example_listfile(_CASTLISTFILE)
    print(u'名单文件模板已经生成到'+_CASTLISTFILE)
      
  #导出默认设置
  def makeconfig(self,command=None):
    print(  u'正在将当前设置保存为'+_CONFIGFILE)
    self.conf.Save_configs(filename=_CONFIGFILE)
    
  def makevideo(self,command=None):
    video=Cast_video(self.GLOBAL_SETTINGS)
    video.Get_page(_PAGEFILE)
    video.Render(_VIDEOFILE)
    
if __name__=='__main__':
  reload(sys)                         # 2
  sys.setdefaultencoding('utf-8')     # 3
  app = CastGenerator()
  app.__main__(sys.argv)

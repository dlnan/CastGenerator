#!/usr/bin/env python2
#coding:UTF-8
#config_handler.py

import ConfigParser, os

#默认的设置文件文件名
_CONF_FILE = 'CastGenerator.conf'
#默认的设置Section
_DEF_SECTION = 'SETTINGS'

class Configs:
  u'处理设置文件的类'
  def __init__(self, configfilename=_CONF_FILE):
    if os.path.isfile(configfilename) :
      self.configs = self.get_from_file(filename=configfilename)
    else:
      self.configs = self.Default_config_dict()
    
  def Default_config_dict(self):
    u'产生默认的设置，返回全局设置字典'
    _settings = {
      #排版相关的变量
      'FONT_DIR' : 'fonts',
      'FONT' : 'SourceHanSansCN-Medium.otf',
      #'FONT' : 'msyh.ttf',
      'JOB_NAME_SPACE' : 1.5, #此处设定间隔多少个汉字,支持小数
      'NAMES_SPACE' : 1, #单行多人名间隔,支持小数
    
      #设置默认图像尺寸
      'VIDEO_WIDTH' : 1280,
      'VIDEO_HEIGHT' : 818,
      'VIDEO_FPS' : 24,
      'VIDEO_FRAMES' : 240,
    
      #页面图片相关内容
    
      'TEXT_BASE_SIZE' : 28,
      'LINE_BASE_SPACE' : 30,
    
      'LOGO_DIR' : 'pics'
      }
    return _settings

  def Save_configs(self, config_dict=None, filename=_CONF_FILE):
    u'储存设置字典为设置文件'
    if config_dict==None:
      config_dict=self.configs
    configs = ConfigParser.RawConfigParser()
    configs.add_section(_DEF_SECTION)
    for key in config_dict.keys():
      configs.set(_DEF_SECTION, key, str(config_dict[key]))
    configfile = open(filename,'wb')
    configs.write(configfile)
    configfile.close()
    
  def get_from_file(self, filename=_CONF_FILE):
    u'从设置文件读取字典'
    FLOAT_CONFIGS = ( 'JOB_NAME_SPACE', 'NAMES_SPACE')
    INT_CONFIGS = ('VIDEO_HEIGHT', 'VIDEO_WIDTH', 'TEXT_BASE_SIZE', 'LINE_BASE_SPACE', 'VIDEO_FPS', 'VIDEO_FRAMES')
    #定义例外的设置名，其它的都是字符串
    #开始读取
    configs = ConfigParser.ConfigParser()
    configs.read(filename)
    final_dict = {}
    for x in configs.items(_DEF_SECTION):
      key = x[0].upper()
      value = x[1]
      if key in INT_CONFIGS :
        value = configs.getint(_DEF_SECTION, key)
      elif key in FLOAT_CONFIGS :
        value = configs.getfloat(_DEF_SECTION, key)
      final_dict.update( { key : value } )
    return final_dict
  
  def timecode2frames(self,timecode,framerate=24):
    u'时间码与帧数换算'
    pass
        
#Get_configs()
  
  
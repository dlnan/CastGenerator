#!/usr/bin/env python2
#coding=UTF-8
#name_list_handler.py


_info = u'''
  本模块是用于处理和名单文本文件相关部分的。
  包含一个名单文件的ExampleFile的生成函数make_example_listfile，
  还有一个专门用来读取名单文件的类Castfile。
  '''

import sys
import os.path
import codecs
from pymodules.little_functions import *

#写入默认名单文件
def make_example_listfile(filename):
  #print( u'没有检测到默认名单文件,即将在当前目录生成。')
  examplefile=[
    u'#以“#”（井号）开头的行是注释行，生成时会自动忽略。',
    u'#以“[]”（中括号）括起来的是小节标题，如“演员表”、“职员表”等。',
    u'#职务/角色与人名之间使用“||”（两个竖杠）隔开，职务在前，人名在后，程序会自动识别。',
    u'[演员表]',
    u'阿三||张三',
    u'狗蛋||李四',
    u'王老五||王五',
    u'路人甲||赵六',
    u'[职员表]',
    u'导演||胡八一',
    u'摄影指导||王胖子',
    u'制片人||斯琴爱儿',
    u'剪辑||Peter Carrol',
    u' ',
    u'#多个人名并排一行的使用“|”（一个竖杠）隔开',
    u'[参加演出]',
    u'谢娜|何炅|汪涵|林正英',
    u'白露|王雪芹|刘欢|李白',
    u' ',
    u'#单位名称两侧加以“=”（等号），字号会比人名略大一些。',
    u'[鸣谢单位]',
    u'=天都市网路视线传媒有限公司=',
    u'=四个眼镜酒店=',
    u'=XX市委宣传部='
    ]
  configfile=codecs.open(filename,'w', DefCode())
  for x in examplefile :
    x += EOL()
    configfile.write(x)
  configfile.close()
  #print( '文件写入完成。文件名为：' + filename )
  
  
class Castfile:
  u'''专门用来读取名单文件的，将会返回一个格式化好的列表。
  列表每一项都有标识符和行内容两项。'''
  def __init__(self,filename):
    self.filename=filename
    _orilines = self.__readlines__(filename)
    self.orilines=_orilines
    self.formatedlines=self.formated_lines()
    
  def formated_lines(self):
    _filteredlines=[]
    for _line in self.orilines:
      if _line[0]=='[' and _line[-1]==']':
        _line=_line[1:-1]
        _filteredline=['TITLE',_line]
        _filteredlines.append(_filteredline)
      elif '||' in _line:
        _jobandname = _line.split('||')
        _filteredline=[ 'JOBANDNAME' , (_jobandname[0] , _jobandname[1]) ]
        _filteredlines.append(_filteredline)
      elif '|' in _line:
        _names = _line.split('|')
        _filteredline=[ 'NAMES' , _names ]
        _filteredlines.append(_filteredline)         
      elif _line[0]=='=' and _line[-1]=='=':
        _line=_line[1:-1]
        _filteredline=['COMPANY',_line]
        _filteredlines.append(_filteredline)
      elif _line[0]=='<' and _line[-1]=='>':
        _line=_line[1:-1]
        _filteredline=['LOGO',_line]
        _filteredlines.append(_filteredline)
      else:
        _filteredline=['ERRORTEXT',_line]
        _filteredlines.append(_filteredline)
    #print _filteredlines
    return _filteredlines
  
  def __readlines__(self,fname):
    _file=open(fname,'r')
    #_lines=_file.readlines()
    #_file.close()
    #return _lines
    _vlines=[]
    for _line in _file.readlines() :
      _line=_line.strip()
      if _line=='' or _line[0]=='#':
        pass
      else:
        _vlines.append(Uni(_line))
    return _vlines
  
if __name__=='__main__':
  print(_info)

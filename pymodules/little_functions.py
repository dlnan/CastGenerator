#!/usr/bin/env python2
#coding:UTF-8
#little_functions.py

from ctypes import *
from ctypes.util import find_library
import os

_info = '''
  本模块用来存放一些用于复用的小功能。
  '''

def Wait_for_press(_exit=False):
  if os.name=='posix' or os.name=='nt':
    libname = find_library('c')
    _c=CDLL(libname)
    
    if _exit==True:
      print(u'按任意键退出。'),
      _c.getchar()
      #_c.press_to_continnue()
    else:
      print(u'按任意键继续……'),
      _c.getchar()
      #_c.press_to_continnue()
  else:
    if _exit==True:
      raw_input(u'按回车键退出。')
    else:
      raw_input(u'按回车键继续……')

    
def Uni(s):
  if os.name=='posix' :
    return s.decode('UTF-8')
  elif os.name=='nt' :
    return s.decode('GBK')
  else:
    return s
  
def DefCode():
  _lookups = {
    'posix' : 'UTF-8' , 
    'nt' : 'GBK'
      }
  return _lookups[os.name]
  #return 'GB2312'
  
def EOL():
  _loopups = {
    'posix' : '\n',
    'nt' : '\r\n'
      }
  return _loopups.get(os.name, '\n')

if __name__ == '__main__' :
  print( _info)
  Wait_for_press(True)
